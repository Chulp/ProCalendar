<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

function procalendar_search($func_vars) {
  extract($func_vars, EXTR_PREFIX_ALL, 'func');

  // how many lines of excerpt we want to have at most
  $max_excerpt_num = $func_default_max_excerpt;
  $divider = ".";
  $result  = false;

  // Set start- and end date for query
  $year  = date('Y', time());
  $month = date('n', time());
  $datestart = "$year-$month-1";
  $dateend = "$year-$month-".cal_days_in_month(CAL_GREGORIAN, $month,$year);

  $table = TABLE_PREFIX."mod_procalendar_actions";
  $query = [];
  $func_database->execute_query("
      SELECT *
      FROM $table
      WHERE section_id='$func_section_id'  
	  	AND date_start <='$dateend' AND date_end >='$datestart' AND public_stat = 0
        ",
        true,
        $query,
        true
    );

  $PageName = $func_page_title;
  
  if(count($query) > 0) 
  {   
    
    foreach($query as $res)
    {
      $text = "";
      
      $text .= $res['name'].$divider.$res['description'].$divider; // Default search: only the WYSIWYG-fields
		//$text .= $res['name'].$divider.$res['description'].$divider.$res['custom1'].$divider.$res['custom2'].$divider.$res['custom3'].$divider; 
	  // Use the line above to add 1, 2 or 3 Custom fields to the search
	  
      $func_page_title = $PageName.":<br/>".$res['name'];
      
      
      $link = "&amp;page_id=".$res['page_id']."&amp;id=".$res['id']."&amp;detail=1";
            
      //$func_page_description = "func_page_description is not used";

      $mod_vars = array
                  (
                    'page_link'          => $func_page_link,
                    'page_link_target'   => $link,
                    'page_title'         => $func_page_title,
                    'page_description'   => $func_page_description,
                    'page_modified_when' => $func_page_modified_when,
                    'page_modified_by'   => $func_page_modified_by,
                    'text'               => $text,
                    'max_excerpt_num'    => $max_excerpt_num
                  );
      
      if(print_excerpt2($mod_vars, $func_vars)) 
      {
        $result = true;
      }
    }
  }
  return $result;
}

?>
