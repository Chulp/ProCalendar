<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

echo '<div class="info"><B>Updating database for module: procalendar</B></div>';

// change fields in version 2.2.1
$database = LEPTON_database::getInstance();
$check = $database->get_one("Select version FROM ".TABLE_PREFIX."addons WHERE name = 'procalendar' ");
if($check < '2.2.1')
{
	$database->simple_query("ALTER TABLE ".TABLE_PREFIX."mod_procalendar_actions CHANGE `acttype` `acttype` INT(4) NOT NULL DEFAULT 0 ");
	$database->simple_query("ALTER TABLE ".TABLE_PREFIX."mod_procalendar_actions CHANGE `public_stat` `public_stat` INT(4) NOT NULL DEFAULT 0 ");
	$database->simple_query("ALTER TABLE ".TABLE_PREFIX."mod_procalendar_actions CHANGE `rec_count` `rec_count` INT(6) NOT NULL DEFAULT 0 ");
	
}

