<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php


//print_r($weekdays);
if(!function_exists("date_to_jscal"))
{
    function date_to_jscal($day, $month, $year, $jscalFormat) {
        $aDate = array(strlen($day)== 1 ? "0".$day : $day,strlen($month)== 1 ? "0".$month : $month,$year); 
        $aFormat = array("d","m","Y"); 
      return str_replace($aFormat, $aDate, $jscalFormat);
    };
}

$rec_day_checked = $rec_week_checked = $rec_month_checked = $rec_year_checked = "";
$rec_exceptions_checked = 'disabled="disabled"';
$wd = array(1=>"mon","tue","wed","thu","fri","sat","sun");
$counter = array(1=>"first","second","third","fourth","last");
for ($i = 1; $i < 8; $i++) {
 ${"rec_week_weekdays_checked".$i} = "";
 ${"rec_month_weekdays_checked_".$wd[$i]} = "";
 ${"rec_year_weekdays_checked_".$wd[$i]} = "";
};
for ($i = 1; $i < 13; $i++) {
 ${"rec_year_option_day_month_selected_".$i} = "";
 ${"rec_year_option_weekday_month_selected_".$i} = "";
}
for ($i = 1; $i < 6; $i++) {
 ${"rec_month_option_selected_".$counter[$i]} = "";
 ${"rec_year_option_selected_".$counter[$i]} = "";
}

$rec_day_days = "";
$rec_week_weeks = "";
$rec_month_days = $rec_month_month = $rec_month_weekday_month ="";
$rec_year_days = "";
$rec_rep_count = $rec_rep_count_checked = "";
$rec_date_exclude1 = $rec_date_exclude2 =	$rec_date_exclude3 = date($jscal_ifformat,$datetime_start);

if(count($ret) > 0)
{
	if ($ret['rec_id'] > 0)
	{
		if ($ret['rec_day'] != "")
		{
			$rec_day_checked='checked="checked"';
			$rec_day_days='value="'.$ret['rec_day'].'"';
		} 
		elseif ($ret['rec_week'] != "")
		{
			$rec_week_checked='checked="checked"';	
			$rec_week = explode("+",$ret['rec_week']);
			$rec_week_weeks = 'value="'.$rec_week[0].'"';
			$rec_weekdays = explode(";",$rec_week[1]);
			foreach($rec_weekdays as $key => $val) 
			{
			${"rec_week_weekdays_checked".$val} = 'checked="checked"';
			}	
		} 
		elseif ($ret['rec_month'] != "")
		{
			$rec_month_checked='checked="checked"';
			$rec_month = explode("+",$ret['rec_month']);
			if (count($rec_month) == 2)
			{  // day - month
				$rec_month_days = 'value="'.$rec_month[0].'"';
				$rec_month_month = 'value="'.$rec_month[1].'"';
			} 
			else 
			{ // week - weekday				
				${"rec_month_option_selected_".$rec_month[0]} = "selected";
				$rec_weekdays = explode(";",$rec_month[1]);
				foreach($rec_weekdays as $key => $val) 
				{
					${"rec_month_weekdays_checked_".$val} = 'checked="checked"';
				}
				$rec_month_weekday_month = 'value="'.$rec_month[2].'"';	
			}	
		} 
		elseif ($ret['rec_year'] != "") 
		{
			$rec_year_checked='checked="checked"';	
			$rec_year = explode("+",$ret['rec_year']);
			if (count($rec_year) == 2)
			{  // day - month
				$rec_year_days = 'value="'.$rec_year[0].'"';
				${"rec_year_option_day_month_selected_".$rec_year[1]} = "selected";
			} 
			else 
			{ // weekday - month	
				${"rec_year_option_selected_".$rec_year[0]} = "selected";
				$rec_weekdays = explode(";",$rec_year[1]);
				foreach($rec_weekdays as $key => $val) 
				{
					${"rec_year_weekdays_checked_".$val} = 'checked="checked"';
				}
				${"rec_year_option_weekday_month_selected_".$rec_year[2]} = "selected";	
			}
		}
		
		if ($ret['rec_count'] == -1)
		{
			 $rec_rep_count_checked='checked="checked"';
		}	 
		elseif ($ret['rec_count'] > 0)
		{
			$rec_rep_count='value="'.$ret['rec_count'].'"';
		}
			
		if ($ret['rec_exclude'] != "")
		{
			$rec_exceptions_checked='checked="checked"';
			$excludes = explode(";",$ret['rec_exclude']);
			for ($i = 0; $i < 4; $i++)
			{
				$excludes[$i] == "" ? $excludes[$i] = "" : $excludes[$i] = date($rec['useifformat'],strtotime($excludes[$i]));
			}
			
			$rec_date_exclude1='value="'.$excludes[0].'"';
			$rec_date_exclude2='value="'.$excludes[1].'"';
			$rec_date_exclude3='value="'.$excludes[2].'"';
		}
	}
}
	
?>
<input id="rec_id" type="hidden" name="rec_id" value="<?php echo (isset($ret['rec_id']) ? $ret['rec_id'] : 0); ?>"/>
<input id="rec_message" type="hidden" name="rec_message" value="<?php echo $MOD_PROCALENDAR['ISREC_MESSAGE']; ?>"/>
<input id="rec_overwrite_message" type="hidden" name="rec_overwrite_message" value="<?php echo $MOD_PROCALENDAR['REC_OVERWRITE_MESSAGE']; ?>"/>
<input id="rec_overwrite" type="hidden" name="rec_overwrite" value="<?php echo $MOD_PROCALENDAR['REC_OVERWRITE']; ?>"/>
<input id="rec_day_called" type="hidden" name="rec_day_called" value="<?php echo date_to_jscal($day,$month,$year,$jscal_ifformat); ?>"/>
<div class="field_line rec_select">
	<div 	class="field_title"><?php echo $MOD_PROCALENDAR['MAKE_REC']; ?></div>
	<input id="rec_day" type="checkbox" name="rec_by[]" <?php echo $rec_day_checked; ?> value="1"/><?php echo $MOD_PROCALENDAR['DAILY']; ?>
	<input id="rec_week" type="checkbox" name="rec_by[]" <?php echo $rec_week_checked; ?> value="2"/><?php echo $MOD_PROCALENDAR['WEEKLY']; ?>
	<input id="rec_month" type="checkbox" name="rec_by[]" <?php echo $rec_month_checked; ?> value="3"/><?php echo $MOD_PROCALENDAR['MONTHLY']; ?>
	<input id="rec_year" type="checkbox" name="rec_by[]" <?php echo $rec_year_checked; ?> value="4"/><?php echo $MOD_PROCALENDAR['YEARLY']; ?>
	<input id="rec_exceptions" type="checkbox" name="rec_by[]" <?php echo $rec_exceptions_checked; ?> value="5"><?php echo $MOD_PROCALENDAR['USE_EXCEPTION']; ?>
</div>
<div class="field_line rec_day procal_hidden">
	<div 	class="field_title" size="3" maxlength="3"><?php echo $MOD_PROCALENDAR['EVERY']; ?></div>
	<input type="text" name="rec_day_days" <?php echo $rec_day_days; ?> size="3" maxlength="3"/>.<?php echo $MOD_PROCALENDAR['DAY']; ?>
</div>
<div class="field_line rec_week procal_hidden">
	<div 	class="field_title"><?php echo $MOD_PROCALENDAR['EVERY_SINGLE']; ?></div>
	<input type="text" name="rec_week_weeks" size="2" maxlength="2" <?php echo $rec_week_weeks; ?> />.<?php echo $MOD_PROCALENDAR['WEEK_ON']; ?>
	<input type="checkbox" name="rec_week_weekday[]" value="1" <?php echo $rec_week_weekdays_checked1; ?> /><?php echo $weekdays[1]; ?>
	<input type="checkbox" name="rec_week_weekday[]" value="2" <?php echo $rec_week_weekdays_checked2; ?> /><?php echo $weekdays[2]; ?>
	<input type="checkbox" name="rec_week_weekday[]" value="3" <?php echo $rec_week_weekdays_checked3; ?> /><?php echo $weekdays[3]; ?>
	<input type="checkbox" name="rec_week_weekday[]" value="4" <?php echo $rec_week_weekdays_checked4; ?> /><?php echo $weekdays[4]; ?>
	<input type="checkbox" name="rec_week_weekday[]" value="5" <?php echo $rec_week_weekdays_checked5; ?> /><?php echo $weekdays[5]; ?>
	<input type="checkbox" name="rec_week_weekday[]" value="6" <?php echo $rec_week_weekdays_checked6; ?> /><?php echo $weekdays[6]; ?>
	<input type="checkbox" name="rec_week_weekday[]" value="7" <?php echo $rec_week_weekdays_checked7; ?> /><?php echo $weekdays[7]; ?>
</div>
<div class="double_field_line rec_month procal_hidden">
	<div 	class="field_title"><?php echo $MOD_PROCALENDAR['EVERY']; ?></div>
	<input type="text" name="rec_month_days" <?php echo $rec_month_days; ?> size="2" maxlength="2"/>.<?php echo $MOD_PROCALENDAR['DAY']; ?>
	<?php echo $MOD_PROCALENDAR['OF_EVERY']; ?>
	<input type="text" name="rec_month_month" <?php echo $rec_month_month; ?> size="2" maxlength="2"/>.<?php echo $MOD_PROCALENDAR['OF_MONATS']; ?><br>
	<div 	class="field_title">&nbsp;</div>
	<select name="rec_month_option_count" size="1">
    <option <?php echo $rec_month_option_selected_first; ?> value="first"><?php echo $MOD_PROCALENDAR['COUNT'][1]; ?></option>
    <option <?php echo $rec_month_option_selected_second; ?> value="second"><?php echo $MOD_PROCALENDAR['COUNT'][2]; ?></option>
    <option <?php echo $rec_month_option_selected_third; ?> value="third"><?php echo $MOD_PROCALENDAR['COUNT'][3]; ?></option>
    <option <?php echo $rec_month_option_selected_fourth; ?> value="fourth"><?php echo $MOD_PROCALENDAR['COUNT'][4]; ?></option>
    <option <?php echo $rec_month_option_selected_last; ?> value="last"><?php echo $MOD_PROCALENDAR['COUNT'][5]; ?></option>
  </select>&nbsp;
	<input type="checkbox" name="rec_month_weekday[]" value="mon" <?php echo $rec_month_weekdays_checked_mon; ?> /><?php echo $weekdays[1]; ?>
	<input type="checkbox" name="rec_month_weekday[]" value="tue" <?php echo $rec_month_weekdays_checked_tue; ?> /><?php echo $weekdays[2]; ?>
	<input type="checkbox" name="rec_month_weekday[]" value="wed" <?php echo $rec_month_weekdays_checked_wed; ?> /><?php echo $weekdays[3]; ?>
	<input type="checkbox" name="rec_month_weekday[]" value="thu" <?php echo $rec_month_weekdays_checked_thu; ?> /><?php echo $weekdays[4]; ?>
	<input type="checkbox" name="rec_month_weekday[]" value="fri" <?php echo $rec_month_weekdays_checked_fri; ?> /><?php echo $weekdays[5]; ?>
	<input type="checkbox" name="rec_month_weekday[]" value="sat" <?php echo $rec_month_weekdays_checked_sat; ?> /><?php echo $weekdays[6]; ?>
	<input type="checkbox" name="rec_month_weekday[]" value="sun" <?php echo $rec_month_weekdays_checked_sun; ?> /><?php echo $weekdays[7]; ?>
	<?php echo $MOD_PROCALENDAR['OF_EVERY']; ?>
	<input type="text" name="rec_month_weekday_month" <?php echo $rec_month_weekday_month; ?> size="2" maxlength="2"/>.<?php echo $MOD_PROCALENDAR['OF_MONATS']; ?>
</div>
<div class="double_field_line rec_year procal_hidden">
	<div 	class="field_title"><?php echo $MOD_PROCALENDAR['EVERY']; ?></div>
  <input type="text" name="rec_year_days" <?php echo $rec_year_days; ?> size="2" maxlength="2"/>.
  <select name="rec_year_option_month" size="1">
    <option <?php echo $rec_year_option_day_month_selected_1; ?> value="1"><?php echo $monthnames[1]; ?></option>
    <option <?php echo $rec_year_option_day_month_selected_2; ?> value="2"><?php echo $monthnames[2]; ?></option>
    <option <?php echo $rec_year_option_day_month_selected_3; ?> value="3"><?php echo $monthnames[3]; ?></option>
	  <option <?php echo $rec_year_option_day_month_selected_4; ?> value="4"><?php echo $monthnames[4]; ?></option>
	  <option <?php echo $rec_year_option_day_month_selected_5; ?> value="5"><?php echo $monthnames[5]; ?></option>
	  <option <?php echo $rec_year_option_day_month_selected_6; ?> value="6"><?php echo $monthnames[6]; ?></option>
	  <option <?php echo $rec_year_option_day_month_selected_7; ?> value="7"><?php echo $monthnames[7]; ?></option>
	  <option <?php echo $rec_year_option_day_month_selected_8; ?> value="8"><?php echo $monthnames[8]; ?></option>
	  <option <?php echo $rec_year_option_day_month_selected_9; ?> value="9"><?php echo $monthnames[9]; ?></option>
	  <option <?php echo $rec_year_option_day_month_selected_10; ?> value="10"><?php echo $monthnames[10]; ?></option>
	  <option <?php echo $rec_year_option_day_month_selected_11; ?> value="11"><?php echo $monthnames[11]; ?></option>
	  <option <?php echo $rec_year_option_day_month_selected_12; ?> value="12"><?php echo $monthnames[12]; ?></option>
  </select><br>
	<div 	class="field_title">&nbsp;</div>
	<select name="rec_year_option_count" size="1">
    <option <?php echo $rec_year_option_selected_first; ?> value="first"><?php echo $MOD_PROCALENDAR['COUNT'][1]; ?></option>
    <option <?php echo $rec_year_option_selected_second; ?> value="second"><?php echo $MOD_PROCALENDAR['COUNT'][2]; ?></option>
    <option <?php echo $rec_year_option_selected_third; ?> value="third"><?php echo $MOD_PROCALENDAR['COUNT'][3]; ?></option>
    <option <?php echo $rec_year_option_selected_fourth; ?> value="fourth"><?php echo $MOD_PROCALENDAR['COUNT'][4]; ?></option>
    <option <?php echo $rec_year_option_selected_last; ?> value="last"><?php echo $MOD_PROCALENDAR['COUNT'][5]; ?></option>
  </select>&nbsp;
	<input type="checkbox" name="rec_year_weekday[]" value="mon" <?php echo $rec_year_weekdays_checked_mon; ?> /><?php echo $weekdays[1]; ?>
	<input type="checkbox" name="rec_year_weekday[]" value="tue" <?php echo $rec_year_weekdays_checked_tue; ?> /><?php echo $weekdays[2]; ?>
	<input type="checkbox" name="rec_year_weekday[]" value="wed" <?php echo $rec_year_weekdays_checked_wed; ?> /><?php echo $weekdays[3]; ?>
	<input type="checkbox" name="rec_year_weekday[]" value="thu" <?php echo $rec_year_weekdays_checked_thu; ?> /><?php echo $weekdays[4]; ?>
	<input type="checkbox" name="rec_year_weekday[]" value="fri" <?php echo $rec_year_weekdays_checked_fri; ?> /><?php echo $weekdays[5]; ?>
	<input type="checkbox" name="rec_year_weekday[]" value="sat" <?php echo $rec_year_weekdays_checked_sat; ?> /><?php echo $weekdays[6]; ?>
	<input type="checkbox" name="rec_year_weekday[]" value="sun" <?php echo $rec_year_weekdays_checked_sun; ?> /><?php echo $weekdays[7]; ?>
	<?php echo $MOD_PROCALENDAR['IN']; ?>
  <select name="rec_year_option_month_weekday" size="1">
    <option <?php echo $rec_year_option_weekday_month_selected_1; ?> value="january"><?php echo $monthnames[1]; ?></option>
    <option <?php echo $rec_year_option_weekday_month_selected_2; ?> value="february"><?php echo $monthnames[2]; ?></option>
    <option <?php echo $rec_year_option_weekday_month_selected_3; ?> value="march"><?php echo $monthnames[3]; ?></option>
	  <option <?php echo $rec_year_option_weekday_month_selected_4; ?> value="april"><?php echo $monthnames[4]; ?></option>
	  <option <?php echo $rec_year_option_weekday_month_selected_5; ?> value="may"><?php echo $monthnames[5]; ?></option>
	  <option <?php echo $rec_year_option_weekday_month_selected_6; ?> value="june"><?php echo $monthnames[6]; ?></option>
	  <option <?php echo $rec_year_option_weekday_month_selected_7; ?> value="july"><?php echo $monthnames[7]; ?></option>
	  <option <?php echo $rec_year_option_weekday_month_selected_8; ?> value="august"><?php echo $monthnames[8]; ?></option>
	  <option <?php echo $rec_year_option_weekday_month_selected_9; ?> value="september"><?php echo $monthnames[9]; ?></option>
	  <option <?php echo $rec_year_option_weekday_month_selected_10; ?> value="october"><?php echo $monthnames[10]; ?></option>
	  <option <?php echo $rec_year_option_weekday_month_selected_11; ?> value="november"><?php echo $monthnames[11]; ?></option>
	  <option <?php echo $rec_year_option_weekday_month_selected_12; ?> value="december"><?php echo $monthnames[12]; ?></option>
  </select>
</div>
<div class="field_line rec_exceptions procal_hidden">
	<div 	class="field_title" size="3" maxlength="3"><?php echo $MOD_PROCALENDAR['NOT_AT']; ?></div>
	<input name="date_exclude1" id="date_exclude1" class="date-pick" <?php echo $rec_date_exclude1; ?>/>
	&nbsp;&nbsp;<input name="date_exclude2" id="date_exclude2" class="date-pick" <?php echo $rec_date_exclude2; ?>/>
	&nbsp;&nbsp;<input name="date_exclude3" id="date_exclude3" class="date-pick" <?php echo $rec_date_exclude3; ?>/>
</div>
