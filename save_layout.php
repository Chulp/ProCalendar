<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php


// Include admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

$aAllFields = [
    "page_id"       => ["type" => "integer", "default" => 0],
    "section_id"    => ["type" => "integer", "default" => 0],
    "header"        => ["type" => "string",  "default" => ""],
    "footer"        => ["type" => "string",  "default" => ""],
    "posttempl"     => ["type" => "string",  "default" => ""]
];
	
$all_values = LEPTON_request::getInstance()->testPostValues($aAllFields);

$database->build_and_execute(
    "update",
    TABLE_PREFIX."mod_procalendar_settings",
    $all_values,
    "`section_id`= ".$all_values['section_id']
);

$admin->print_success(
    $TEXT['SUCCESS'],
    LEPTON_URL."/modules/procalendar/modify_settings.php?page_id=".$all_values['page_id']."&section_id=".$all_values['section_id']."&leptoken=".get_leptoken()
);

