<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

if(class_exists("lib_comp", true))
{
    lib_comp::init("procalendar");
}
//die(LEPTON_tools::display_dev($_POST, 'pre','ui blue message',true));
$update_when_modified = true;
require(LEPTON_PATH.'/modules/admin.php');
//die(LEPTON_tools::display_dev($_POST,'pre','ui message',true));	
/**
 * Aldus 2023-03-03: Problem here is that some values are not postet when the
 *  basic settings are not set. E.g. time start will only be here if the settings
 *  have not been set to "don't use time"!
 *
 * @todo Use LEPTON_request instead.
 */
$out = "";
$cal_id         =  LEPTON_core::getValue('cal_id');
$deleteaction   = $admin->getValue('delete');
$SaveAsNew      = $admin->getValue('saveasnew');
$overwrite      = $admin->getValue('overwrite');
$edit_overwrite = $admin->getValue('edit_overwrite');
$js_start_time  = $admin->getValue('date1') ?? "01.01.1971"; // date 1
$js_end_time    = $admin->getValue('date2') ?? "01.01.1971"; // date 2
$time_start     = $admin->getValue('time_start') ?? "00:00"; // time start
$time_end       = $admin->getValue('time_end') ?? "00:00";   // time end
$jscal_format   = $admin->getValue('jscal_format');
$rec_by			= $admin->getValue('rec_by');
$rec_id_overwrite	= $admin->getValue('rec_id');
$rec_id = $rec_count = 0;
$rec_type = $rec_days = $rec_weeks = $rec_months = $rec_years = $rec_exclude = "";
$jscalFormat = preg_split( "#\s|/|\.|-#", $jscal_format);

// make sure that $rec_count has a value, see line 129 
$iTemp = '0';
if ($rec_by > 0 )
{
	$iTemp = '1';	
}

//formats jscal date string to standard mysql format
function jscal_to_date($strDate, $jscalFormat) {
  $formatedDate = "";
	if ($strDate != ""){
	  $dateParts = preg_split( "#\s|/|\.|-#", $strDate);
	  $dateCombined = array_combine($jscalFormat, $dateParts);
	  $formatedDate = $dateCombined['yy']."-".$dateCombined['mm']."-".$dateCombined['dd'];
	};
	return $formatedDate;
};

if (isset($overwrite)) {
	$SaveAsNew = 1;
	$rec_id = $rec_id_overwrite;
};

if (isset($edit_overwrite)) $rec_id = $rec_id_overwrite;

if (isset($rec_by)) { 
	$rec_id = time();
	switch ($rec_by[0]) {
    case 1:
        $rec_type = "rec_day";
        $rec_days =  '1'; //rec_day must be 1		$admin->getValue('rec_day_days');
        break;
    case 2:
        $rec_type = "rec_week";
        $rec_weeks = $admin->getValue('rec_week_weeks').'+';
/* gsm 20240310 add */
		$rec_weeks .= is_array ( $admin->getValue('rec_week_weekday')) 
			? implode ( ";", $admin->getValue('rec_week_weekday'))
			: $admin->getValue('rec_week_weekday');
        break;
    case 3:
        $rec_type = "rec_month";
        if ($admin->getValue('rec_month_days') != "") {
	        $rec_months = $admin->getValue('rec_month_days').'+';
	        $rec_months .= $admin->getValue('rec_month_month');
	      } else {
	        $rec_months = $admin->getValue('rec_month_option_count').'+';
	        $rec_months .= implode(";",$admin->getValue('rec_month_weekday')).'+';  
	        $rec_months .= $admin->getValue('rec_month_weekday_month');   	
	      }
        break;
    case 4:
        $rec_type = "rec_year";
        if ($admin->getValue('rec_year_days') != "") {
	        $rec_years = $admin->getValue('rec_year_days').'+';
	        $rec_years .= $admin->getValue('rec_year_option_month');
	      } else {
	        $rec_years = $admin->getValue('rec_year_option_count').'+';
	        $rec_years .= implode(";",$admin->getValue('rec_year_weekday')).'+'; 
	        $rec_years .= $admin->getValue('rec_year_option_month_weekday');  	
	      }
        break;
	};
	
	if (count($rec_by) == 2) {
		for ($i=1; $i < 4; $i++)
			$rec_exclude .= jscal_to_date($admin->getValue('date_exclude'.$i), $jscalFormat).';';
	};
	
	// make sure that $rec_count has a value
	$rec_count = intval($admin->getValue('rec_rep_count'));
	if($rec_count < 1)
	{
		$rec_count = 1;
	}
	
	$rec_never = $admin->getValue('rec_never');
	if (isset($rec_never))
	  $rec_count = -1;

}


// Added PCWacht
// Didn't think of anything nicer, but this works as well
// First get first letter of date format, since we have only 3 choices, d, m, Y and rebuild date as yyyy-mm-dd
$format = substr($jscal_format,0,1);
if ($format == 'd') {
	$js_end_day    = substr($js_end_time ?? '01.01.1980',0,2);
	$js_end_month  = substr($js_end_time ?? '01.01.1980',3,2);
	$js_end_year   = substr($js_end_time ?? '01.01.1980',6,4);

	$js_start_day    = substr($js_start_time ?? '01.01.1980',0,2);
	$js_start_month  = substr($js_start_time ?? '01.01.1980',3,2);
	$js_start_year   = substr($js_start_time ?? '01.01.1980',6,4);
	
} elseif ($format == 'm') { 
	$js_end_day    = substr($js_end_time ?? '01.01.1980',3,2);
	$js_end_month  = substr($js_end_time ?? '01.01.1980',0,2);
	$js_end_year   = substr($js_end_time ?? '01.01.1980',6,4);

	$js_start_day    = substr($js_start_time ?? '01.01.1980',3,2);
	$js_start_month  = substr($js_start_time ?? '01.01.1980',0,2);
	$js_start_year   = substr($js_start_time ?? '01.01.1980',6,4);
	
} else { 
	$js_end_day    = substr($js_end_time ?? '01.01.1980',8,2);
	$js_end_month  = substr($js_end_time ?? '01.01.1980',5,2);
	$js_end_year   = substr($js_end_time ?? '01.01.1980',0,4);

	$js_start_day    = substr($js_start_time ?? '01.01.1980',8,2);
	$js_start_month  = substr($js_start_time ?? '01.01.1980',5,2);
	$js_start_year   = substr($js_start_time ?? '01.01.1980',0,4);
}	

$date_start  = "$js_start_year-$js_start_month-$js_start_day";
$date_end    = "$js_end_year-$js_end_month-$js_end_day";
$date_start = (jscal_to_date($js_start_time, $jscalFormat));
$date_end = (jscal_to_date($js_end_time, $jscalFormat));

  	
if (isset($deleteaction)) 
{
	//if recurring, delete all overwrites too
	$sql = "SELECT * FROM ".TABLE_PREFIX."mod_procalendar_actions WHERE id = ".$cal_id;
	$db = $database->query($sql);
	$rec = $db->fetchRow();
	if ($rec['rec_id'] > 0 && ($rec['rec_day'] != "" || $rec['rec_week'] != "" || $rec['rec_month'] != "" || $rec['rec_year'] != ""))
	{
		$sql = "DELETE FROM ".TABLE_PREFIX."mod_procalendar_actions WHERE rec_id = ".$rec['rec_id'];
	}
	else
	{
		$sql = "DELETE FROM ".TABLE_PREFIX."mod_procalendar_actions WHERE id = ".$cal_id;
	}

	$database->query($sql);
	if($database->is_error())
	{
		die(LEPTON_tools::display($database->get_error(), 'pre','ui blue message'));
	}

} 
else 
{

  $sql = "SELECT * FROM ".TABLE_PREFIX."mod_procalendar_settings WHERE section_id = ".$section_id;
  $db = $database->query($sql);
  if ($db->numRows() > 0) 
  	{
  		$rec = $db->fetchRow();
  		// Added PCWacht
  		// Need to invers the firstday for calendar   
  		$use_time       = $rec['usetime'];
		$onedate        = $rec["onedate"];
		$usecustom1     = $rec["usecustom1"];
		$usecustom2     = $rec["usecustom2"];
		$usecustom3     = $rec["usecustom3"];
		$usecustom4     = $rec["usecustom4"];
		$usecustom5     = $rec["usecustom5"];
		$usecustom6     = $rec["usecustom6"];
		$usecustom7     = $rec["usecustom7"];
		$usecustom8     = $rec["usecustom8"];
		$usecustom9     = $rec["usecustom9"];
		$resize         = $rec["resize"];
	} 
	
  $short = $admin->getValue('short','string_chars','post');
  $long = $admin->getValue('long','string_chars','post');
  
  if (isset($SaveAsNew)) 
   $cal_id=0;
  else
   $cal_id = $admin->getValue('cal_id');
  
  $section_id  = $admin->getValue('section_id');
  $page_id     = $admin->getValue('page_id');
  $name        = htmlspecialchars($admin->getValue('name'));
  
  // Check if the user uploaded an image 
  function checkimage($checkname, $custom) {
    global $admin, $resize, $MESSAGE;
    if ($custom == '0') $custom='';	  
    if(isset($_FILES[$checkname]['tmp_name']) AND $_FILES[$checkname]['tmp_name'] != '')
    {
  	  // Get real filename and set new filename
	  $filename = $_FILES[$checkname]['name'];
	  $new_filename = LEPTON_PATH.MEDIA_DIRECTORY.'/calendar/'.$filename;
	  $st_filename =  LEPTON_URL.MEDIA_DIRECTORY.'/calendar/'.$filename;
	  // Make kinda sure the image is an image - there should be something better than just to test extention
	  $file4=strtolower(substr($filename, -4, 4));
	  if(($file4 != '.jpg')and($file4 != '.png')and($file4 !='jpeg') )
      {
		$admin->print_error($MESSAGE['GENERIC_FILE_TYPE'].' JPG (JPEG) or PNG a');
	  }
	  // Make sure the target directory exists
	  LEPTON_core::make_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/calendar');
	  // Upload image
	  move_uploaded_file($_FILES[$checkname]['tmp_name'], $new_filename);
	  // Check if we need to create a thumb
	  if($resize != 0)
      {
		// Resize the image
		$thumb_location = LEPTON_PATH.MEDIA_DIRECTORY.'/calendar/thumb'.$filename.'.jpg';
		if(make_thumb($new_filename, $thumb_location, $resize))
        {
			// Delete the actual image and replace with the resized version
			unlink($new_filename);
			rename($thumb_location, $new_filename);
		}
	  }
	  return $st_filename;
    }
    return $custom;
  }    
  
  if ($usecustom1 <> 0) $custom1 = $admin->getValue('custom1'); 
  if ($usecustom1 == 4) $custom1 = checkimage('custom_image1', $custom1 );
  if ($usecustom2 <> 0) $custom2 = $admin->getValue('custom2'); 
  if ($usecustom2 == 4) $custom2 = checkimage('custom_image2', $custom2);
  if ($usecustom3 <> 0) $custom3 = $admin->getValue('custom3'); 
  if ($usecustom3 == 4) $custom3 = checkimage('custom_image3', $custom3);
  if ($usecustom4 <> 0) $custom4 = $admin->getValue('custom4'); 
  if ($usecustom4 == 4) $custom4 = checkimage('custom_image4', $custom4);
  if ($usecustom5 <> 0) $custom5 = $admin->getValue('custom5'); 
  if ($usecustom5 == 4) $custom5 = checkimage('custom_image5', $custom5);
  if ($usecustom6 <> 0) $custom6 = $admin->getValue('custom6'); 
  if ($usecustom6 == 4) $custom6 = checkimage('custom_image6', $custom6);
  if ($usecustom7 <> 0) $custom7 = $admin->getValue('custom7'); 
  if ($usecustom7 == 4) $custom7 = checkimage('custom_image7', $custom7);
  if ($usecustom8 <> 0) $custom8 = $admin->getValue('custom8'); 
  if ($usecustom8 == 4) $custom8 = checkimage('custom_image8', $custom8);
  if ($usecustom9 <> 0) $custom9 = $admin->getValue('custom9'); 
  if ($usecustom9 == 4) $custom9 = checkimage('custom_image9', $custom9);

  $acttype     = $admin->getValue('acttype');
  $public_stat = $admin->getValue('public_stat');

  if (strlen($date_start) == 0) 
  {
    $date_start = date("Y-m-d");
  }
  
  if (strlen($time_start) == 0) 
  {
    $time_start = "00:00";
  } 
  

  if((int)$js_end_day == 0 || (int)$js_end_month == 0 || (int)$js_end_year == 0)
  {
    $date_end = $date_start;
  }
  else 
  {
    $date_end = $date_end;
  }
  
  if (strlen($time_end) == 0) 
  {
    $time_end = "00:00";
  } 
  
  if (!$onedate)
    $date_end = $date_start;

  // Check dates, make end equal to start if start > end
  $begin = $date_start.' '.$time_start;
  $end   = $date_end.' '.$time_end;
  if ($begin>$end) {
	  $date_end = $date_start;
	  $time_end = $time_start;
  }	  
     
  $description = $admin->getValue('description');
  $owner       = $admin->getValue('owner');
  
  if (empty($owner))
  {
    $owner = 1;
  }
  
  if(trim($name)!="")
  {
    if ($cal_id==0) 
    {
      $sql = "INSERT INTO ".TABLE_PREFIX."mod_procalendar_actions SET ";
      $sql .= "section_id=".$section_id.", ";
      $sql .= "page_id=".$page_id.", ";
      $sql .= "owner=".$owner.", ";
      $sql .= "name='".$name."', ";
	  if ($usecustom1 <> 0) $sql .= "custom1='$custom1', ";
	  if ($usecustom2 <> 0) $sql .= "custom2='$custom2', ";
	  if ($usecustom3 <> 0) $sql .= "custom3='$custom3', ";
	  if ($usecustom4 <> 0) $sql .= "custom4='$custom4', ";
	  if ($usecustom5 <> 0) $sql .= "custom5='$custom5', ";
	  if ($usecustom6 <> 0) $sql .= "custom6='$custom6', ";
	  if ($usecustom7 <> 0) $sql .= "custom7='$custom7', ";
	  if ($usecustom8 <> 0) $sql .= "custom8='$custom8', ";
	  if ($usecustom9 <> 0) $sql .= "custom9='$custom9', ";
      $sql .= "acttype=".$acttype.", ";
      $sql .= "public_stat='$public_stat', ";
      $sql .= "date_start='$date_start', ";
      $sql .= "date_end='$date_end', ";
      $sql .= "time_start='$time_start', ";
      $sql .= "time_end='$time_end', ";
      $sql .= "description='$short', ";
      $sql .= "rec_id='$rec_id', ";
      $sql .= "rec_day='$rec_days', ";
      $sql .= "rec_week='$rec_weeks', ";
      $sql .= "rec_month='$rec_months', ";
      $sql .= "rec_year='$rec_years', ";
      $sql .= "rec_count=".$rec_count.", ";
      $sql .= "rec_exclude='$rec_exclude'";
    } 
    else 
    {
      $sql = "UPDATE ".TABLE_PREFIX."mod_procalendar_actions SET ";
      $sql .= "name='$name', ";
	  if ($usecustom1 <> 0) $sql .= "custom1='$custom1', ";
	  if ($usecustom2 <> 0) $sql .= "custom2='$custom2', ";
	  if ($usecustom3 <> 0) $sql .= "custom3='$custom3', ";
	  if ($usecustom4 <> 0) $sql .= "custom4='$custom4', ";
	  if ($usecustom5 <> 0) $sql .= "custom5='$custom5', ";
	  if ($usecustom6 <> 0) $sql .= "custom6='$custom6', ";
	  if ($usecustom7 <> 0) $sql .= "custom7='$custom7', ";
	  if ($usecustom8 <> 0) $sql .= "custom8='$custom8', ";
	  if ($usecustom9 <> 0) $sql .= "custom9='$custom9', ";
      $sql .= "acttype=".$acttype.", ";
      $sql .= "public_stat='$public_stat', ";
      $sql .= "date_start='$date_start', ";
      $sql .= "date_end='$date_end', ";
      $sql .= "time_start='$time_start', ";
      $sql .= "time_end='$time_end', ";
      $sql .= "description='$short', ";
      $sql .= "rec_day='$rec_days', ";
      $sql .= "rec_week='$rec_weeks', ";
      $sql .= "rec_month='$rec_months', ";
      $sql .= "rec_year='$rec_years', ";
      $sql .= "rec_count=".$rec_count.", ";
      $sql .= "rec_exclude='$rec_exclude' ";
      $sql .= "WHERE id='$cal_id'";
    }

    $database->query($sql);
  }
}
require_once('functions.php');

$admin->print_success($MESSAGE['PAGES_SAVED'], ADMIN_URL.'/pages/modify.php?page_id='."$page_id&month=$js_start_month&year=$js_start_year");

