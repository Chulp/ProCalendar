<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

global $monthnames,$weekdays,$public_stat;
$monthnames = [1=>"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
$weekdays = [1=>"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"];
$public_stat = ["public", "private", "logged in"];

$MOD_PROCALENDAR = [
	'CAL-OPTIONS' => "Options",
	'CAL-OPTIONS-STARTDAY' => "Start weekday",
	'CAL-OPTIONS-STARTDAY-1' => "Monday",
	'CAL-OPTIONS-STARTDAY-2' => "Sunday",
	'CAL-OPTIONS-USETIME' => "Times",
	'CAL-OPTIONS-USETIME-1' => "Don't use times",
	'CAL-OPTIONS-USETIME-2' => "Use times",
	'CAL-OPTIONS-FORMAT' => "Date format",
	'CAL-OPTIONS-ONEDATE' => "Date",
	'CAL-OPTIONS-ONEDATE-1' => "Use Start date only",
	'CAL-OPTIONS-ONEDATE-2' => "Use Start date &amp; End date",
	'CATEGORY-MANAGEMENT' => "Category management",
	'CATEGORY' => "Category",
	'CHOOSE-CATEGORY' => "Category...",
	'ACTIVE' => "Active",
	'BACK' => "&laquo; Back",
	'DATE-AND-TIME' => "Date and time",
	'NOTIME' => "No time available...",
	'NODATES' => "No dates available...",
	'NODETAILS' => "No details available...",
	'TIMESTR' => "o'clock",
	'DEADLINE' => "End",
	'DELETE' => "Delete",
	'DESCRIPTION' => "Description",
	'NO_DESCRIPTION' => "No description available...",
	'FROM' => "Start",
	'NEW-EVENT' => "New event",
	'NEW' => "New",
	'NON-SPECIFIED' => "N/A",
	'NAME' => "Name",
	'SAVE' => "Save",
	'VISIBLE' => "Visibility",
	'SETTINGS' => "Settings",
	'ADVANCED-SETTINGS' => "Advanced settings",
	'SAVE-AS-NEW' => "Save as new",
	'TO' => "End",
	'USE-THIS' => "Use this",
	'CALENDAR-DEFAULT-TEXT' => "", // put default item title here if you like
	'CALENDAR-BACK-MONTH' => "Monthly view",
	'DATE' => "Date",
	'CUSTOMS' => "Custom fields",
	'CUSTOM' => "Custom field",
	'CUSTOM_TEMPLATE' => "Field template",
	'USE_CUSTOM' => "Type",
	'CUSTOM_NUMBER' => "Field number",
	'CUSTOM_TYPE' => "Field type",
	'CUSTOM_NAME' => "Field name",
	'CUSTOM_OPTIONS-0' => "Unused",
	'CUSTOM_OPTIONS-1' => "Text field",
	'CUSTOM_OPTIONS-2' => "Text area",
	'CUSTOM_OPTIONS-3' => "Link",
	'CUSTOM_OPTIONS-4' => "Image",
	'CUSTOM_SELECT_IMG' => "Select image",
	'CUSTOM_CHOOSE_IMG' => "No image",
	'CUSTOM_SELECT_LEPTONLINK' => "Select page",
	'RESIZE_IMAGES' => "Resize images",
	'SUPPORT_INFO' => "Support information",
	'SUPPORT_INFO_INTRO' => "Before using this module, please read the ",
	'FORMAT_ACTION' => "You can set a colour for each category by clicking on the coloured ball",
	'FORMAT_DAY' => "Use this color in Calendar?",
	'MAKE_REC' => "Recurrent Date?",
	'DAILY' => "daily",
	'WEEKLY' => "weekly",
	'MONTHLY' => "monthly",
	'YEARLY' => "yearly",
	'DAYS' => "days",
	'DAY' => "day",
	'EVERY' => "every",
	'EVERY_SINGLE' => "every",
	'WEEK_ON' => "week on",
	'OF_EVERY' => "of every",
	'OF_MONATS' => "month",
	'IN' => "in",
	'COUNT' => array(1=>"first", "second", "third", "fourth", "last"),
	'NOT_AT' => "Not at",
	'USE_EXCEPTION' => "Use exceptions?",
	'DATES' => "dates",
	'NEVER' => "never",
	'ISREC_MESSAGE' => "This date is part of a date series. /n&quot;OK&quot; - for editing the whole series. /n &quot;Cancel&quot; - for overwriting date or creating a new one.",
	'REC_OVERWRITE_MESSAGE' => "This Date is part of a date series. \nIf you delete this date the original date of the series will become active again.",
	'REC_OVERWRITE' => "overwrite",
];
