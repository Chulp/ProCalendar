<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

global $monthnames,$weekdays,$public_stat;
$monthnames = [1=>"Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"];
$weekdays = [1=>"Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"];
$public_stat = ["öffentlich", "privat", "eingeloggt"];

$MOD_PROCALENDAR = [
	'CAL-OPTIONS' => "Optionen",
	'CAL-OPTIONS-STARTDAY' => "Erster Wochentag",
	'CAL-OPTIONS-STARTDAY-1' => "Montag",
	'CAL-OPTIONS-STARTDAY-2' => "Sonntag",
	'CAL-OPTIONS-USETIME' => "Uhrzeit",
	'CAL-OPTIONS-USETIME-1' => "Uhrzeit nicht verwenden",
	'CAL-OPTIONS-USETIME-2' => "Uhrzeit verwenden",
	'CAL-OPTIONS-FORMAT' => "Datumsformat",
	'CAL-OPTIONS-ONEDATE' => "Datum",
	'CAL-OPTIONS-ONEDATE-1' => "Nur Startdatum verwenden",
	'CAL-OPTIONS-ONEDATE-2' => "Start- und Enddatum verwenden",
	'CATEGORY-MANAGEMENT' => "Kategorien verwalten",
	'CATEGORY' => "Kategorie",
	'CHOOSE-CATEGORY' => "Kategorie...",
	'ACTIVE' => "Aktiv",
	'BACK' => "Zurück",
	'DATE-AND-TIME' => "Datum und Uhrzeit",
	'NOTIME' => "Keine Uhrzeit vorhanden...",
	'NODATES' => "Keine Einträge vorhanden...",
	'NODETAILS' => "Keine Details vorhanden...",
	'TIMESTR' => "Uhr",
	'DEADLINE' => "Ende",
	'DELETE' => "Löschen",
	'DESCRIPTION' => "Beschreibung",
	'NO_DESCRIPTION' => "Keine Beschreibung verfügbar...",
	'FROM' => "Von",
	'NEW-EVENT' => "Neuer Termin",
	'NEW' => "Neu",
	'NON-SPECIFIED' => "nichts hinterlegt",
	'NAME' => "Bezeichnung",
	'SAVE' => "Speichern",
	'VISIBLE' => "Sichtbarkeit",
	'SETTINGS' => "Optionen",
	'ADVANCED-SETTINGS' => "Erweiterte Optionen",
	'SAVE-AS-NEW' => "Als neu speichern",
	'TO' => "Ende",
	'USE-THIS' => "Verwenden",
	'CALENDAR-DEFAULT-TEXT' => "", // put default item title here if you like
	'CALENDAR-BACK-MONTH' => "Monatsübersicht",
	'DATE' => "Datum",
	'CUSTOMS' => "Eigene Felder",
	'CUSTOM' => "Eigenes Feld",
	'CUSTOM_TEMPLATE' => "Feld-Template",
	'USE_CUSTOM' => "Typ",
	'CUSTOM_NUMBER' => "Feld Nummer",
	'CUSTOM_TYPE' => "Feldtyp",
	'CUSTOM_NAME' => "Feldbezeichnung",
	'CUSTOM_OPTIONS-0' => "Nicht benutzt",
	'CUSTOM_OPTIONS-1' => "Textfeld",
	'CUSTOM_OPTIONS-2' => "Textarea",
	'CUSTOM_OPTIONS-3' => "Link",
	'CUSTOM_OPTIONS-4' => "Bild",
	'CUSTOM_SELECT_IMG' => "Bild auswählen",
	'CUSTOM_CHOOSE_IMG' => "Kein Bild",
	'CUSTOM_SELECT_LEPTONLINK' => "Seite auswählen",
	'RESIZE_IMAGES' => "Bildgröße ändern",
	'SUPPORT_INFO' => "Hinweise zum Modul",
	'SUPPORT_INFO_INTRO' => "Bitte lies vor Verwendung des Moduls die ",
	'FORMAT_ACTION' => "Mit dem Ballsymbol können Sie eine Farbe für die Kategorie festlegen",
	'FORMAT_DAY' => "Diese Farbe im Kalender benutzen?",
	'MAKE_REC' => "Wiederholender Termin?",
	'DAILY' => "täglich",
	'WEEKLY' => "wöchentlich",
	'MONTHLY' => "monatlich",
	'YEARLY' => "jährlich",
	'DAYS' => "Tage",
	'DAY' => "Tag",
	'EVERY' => "Jeden",
	'EVERY_SINGLE' => "Jede",
	'WEEK_ON' => "Woche am",
	'OF_EVERY' => "jedes",
	'OF_MONATS' =>"Monats",
	'IN' =>"im",
	'COUNT' => array(1=>"ersten", "zweiten", "dritten", "vierten", "letzten"),
	'NOT_AT' =>"Nicht am",
	'USE_EXCEPTION' => "Ausnahmen zulassen?",
	'DATES' => "Termine",
	'NEVER' => "niemals",
	'ISREC_MESSAGE' => "Dieser Termin ist Teil einer Terminserie. \n&quot,OK&quot, - um die ganze Serie zu ändern. \n&quot,Abbrechen&quot, - um den Termin zu überschreiben oder einen neuen Termin anzulegen.",
	'REC_OVERWRITE_MESSAGE' => "Dieser Temin überschreibt eine Terminserie. \nWenn Sie diesen Termin löschen, wird der ursprüngliche Termin der Serie wieder aktiv.",
	'REC_OVERWRITE' => "überschreiben"
];
