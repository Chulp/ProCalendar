<?php 

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php


require_once 'functions.php';

$oPCFE = procalendar_frontend::getInstance();
$oPCFE->init_addon($page_id, $section_id);
//echo(LEPTON_tools::display($oPCFE,'pre','ui message'));

global $oLEPTON, $day, $month, $year, $action_types, $monthnames, $weekdays, $MOD_PROCALENDAR;

$MOD_PROCALENDAR = $oPCFE->language;

$day = isset($_GET['day']) ? (int)$_GET['day'] : "";
$dayview = isset($_GET['dayview']) ? (int)$_GET['dayview'] : 0;
$month = isset($_GET['month']) ? (int)$_GET['month'] : date("n");
$year = isset($_GET['year']) ? (int)$_GET['year'] : date("Y");
$show = isset($_GET['show']) ? (int)$_GET['show'] : 0;
$detail = isset($_GET['detail']) ? (int)$_GET['detail'] : 0;
$id = isset($_GET['id']) ? (int)$_GET['id'] : 0;
// range for a month
$date_start = "$year-$month-1";
$date_end = "$year-$month-".DaysCount($month,$year);

($month > 1) ? ($prevmonth = $month - 1) :  ($prevmonth = 12);
($month < 12) ? ($nextmonth = $month + 1) :  ($nextmonth = 1);
($month == 1) ? ($prevyear = $year - 1) : ($prevyear = $year);
($month == 12) ? ($nextyear = $year + 1) : ($nextyear = $year);

$actions  = fillActionArray($date_start, $date_end, $section_id);
if (!is_array($actions)) 
{
    $actions = [];
}
$action_types = fillActionTypes($section_id);

if ($detail == 1)
{
    if ($id == 0)
    {
        ShowActionDetails($actions, $section_id, $day, $month, $year, $show, $dayview);
    }
    else
    {
        ShowActionDetailsFromId($actions, $id, $section_id, $day);
    }
}
else
{
    ShowCalendar($month,$year,$actions,$section_id,false);
    ShowActionList($day,$month,$year,$actions,$section_id);
}