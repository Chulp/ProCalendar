<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

function years(){
  $i=date('Y');
  $end=$i+10;
  while ($i <= $end) { 
    echo"<option value=\"$i\">$i</option>";
    $i++; 
  }
}

function years_preselect(){
  $i=date('Y');
  echo"<option selected value=\"$i\">$i</option>";
  $i++;
  $end=$i+10;
  while ($i <= $end) { 
    echo"<option value=\"$i\">$i</option>";
    $i++; 
  }
}

function days(){
  $i=1;
  while ($i <= 31) { 
    echo'<option value="';
    printf("%002s",  $i);
    echo"\">$i</option>";
    $i++; 
  }
}

function days_preselect(){
  $i=1;
  $m=date('d');
  while ($i <= 31) { 
    if($i == $m)
      echo'<option selected value="';
    else
      echo'<option value="';
    printf("%002s",  $i);
    echo"\">$i</option>";
    $i++; 
  }
}

function months(){
  $i=1;
  while ($i <= 12) { 
    echo'<option value="';
    printf("%002s",  $i);
    echo"\">$i</option>";
    $i++; 
  }
}

function months_preselect(){
  $i=1;
  $m=date('m');
  while ($i <= 12) { 
    if($i == $m)
      echo'<option selected value="';
    else
      echo'<option value="';
    printf("%002s",  $i);
    echo"\">$i</option>";
    $i++; 
  }
}

function hours(){
  $i=0;
  while ($i <= 23) { 
    echo'<option value="';
    printf("%002s",  $i);
    echo"\">$i</option>";
    $i++; 
  }
}

function minutes(){
  $i=0;
  while ($i <= 59) { 
    echo'<option value="';
    printf("%002s",  $i);
    echo"\">$i</option>";
    $i++; 
  }
}
