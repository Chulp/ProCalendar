<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

// Include admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

$type         = $admin->getValue('type');
$page_id      = $admin->getValue('page_id');
$section_id   = $admin->getValue('section_id');

switch ($type) {

case "change_eventgroup":
	$group_id    = $admin->getValue('group_id');
	$group_name  = $admin->getValue('group_name');
	$delete      = $admin->getValue('delete');
	$format 	   = $admin->getValue('action_background');
	$dayformat 	 = $admin->getValue('dayformat');
	if (!isset($dayformat)) $dayformat = 0;

	if ($delete) 
  {
		$sql = "DELETE FROM ".TABLE_PREFIX."mod_procalendar_eventgroups WHERE id=$group_id";
		$database->query($sql);
	} 
	else 
  {
    if($group_name != "") 
    {
 			if (($group_id == 0)) 
      {
        //echo "INSERT -> page_id: $page_id - group_name: $group_name  <br>";
				$sql = "INSERT INTO ";	
				$sql .= TABLE_PREFIX."mod_procalendar_eventgroups SET ";	
				$sql .= "section_id='$section_id', ";
				$sql .= "name='$group_name', ";
				$sql .= "format='$format', ";
				$sql .= "format_days='$dayformat' ";
			} 
      else 
      {
        //echo "UPDATE -> group_id: <br>"; 
				$sql = "UPDATE ";
				$sql .= TABLE_PREFIX."mod_procalendar_eventgroups SET ";	
				$sql .= "section_id='$section_id', ";
				$sql .= "name='$group_name', ";
				$sql .= "format='$format', ";
				$sql .= "format_days='$dayformat' ";
				$sql .= " WHERE id=$group_id";
			}
			
			$database->query($sql);
		}
	}
	break;
		
	case "startd":
		$startday     = $admin->getValue('startday');
		$onedate      = $admin->getValue('onedate');
		$usetime      = $admin->getValue('usetime');
		$useformat    = $admin->getValue('useformat');
		switch ($useformat) {
		  case "dd.mm.yy":
		     $useifformat = "d.m.Y";
			 break;
		  case "dd-mm-yy":
		     $useifformat = "d-m-Y";
			 break;
		  case "dd/mm/yy":
		     $useifformat = "d/m/Y";
			 break;
		  case "dd mm yy":
		     $useifformat = "d m Y";
			 break;
		  case "mm.dd.yy":
		     $useifformat = "m.d.Y";
			 break;
		  case "mm-dd-yy":
		     $useifformat = "m-d-Y";
			 break;
		  case "mm/dd/yy":
		     $useifformat = "m/d/Y";
			 break;
		  case "mm dd yy":
		     $useifformat = "m d Y";
			 break;
		  case "yy.mm.dd":
		     $useifformat = "Y.m.d";
			 break;
		  case "yy-mm-dd":
		     $useifformat = "Y-m-d";
			 break;
		  case "yy/mm/dd":
		     $useifformat = "Y/m/d";
			 break;
		  case "yy mm dd":
		     $useifformat = "Y m d";
			 break;
		  default:
		     $useifformat = "Y/m/d";
		}

		$sql = "UPDATE ";
        $sql .= TABLE_PREFIX."mod_procalendar_settings SET "; // create rest of the sql-query
        $sql .= "startday='$startday', ";
        $sql .= "usetime='$usetime', ";
        $sql .= "onedate='$onedate', ";
        $sql .= "useformat='$useformat', ";
        $sql .= "useifformat='$useifformat' ";
        $sql .= " WHERE section_id=$section_id";
      
        $database->query($sql);
	break;  		
}

if($database->is_error()) {
  $admin->print_error($database->get_error(), $js_back);
} else {
	if ($type == "change_eventgroup" ) { 
	  $admin->print_success($TEXT['SUCCESS'], LEPTON_URL."/modules/procalendar/modify_settings.php?page_id=".$page_id."&section_id=".$section_id);
	} else {
	  $admin->print_success($MESSAGE['PAGES_SAVED'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
	}
}


$admin->print_footer();

?>