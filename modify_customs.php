<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

// Include admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

$lang_file = __DIR__.'/languages/'.LANGUAGE .'.php';
require_once( file_exists($lang_file) ? $lang_file : __DIR__.'/languages/EN.php');

$fillvalue = "";

// Added PCWacht
// moved to one place
// Fetch needed settings from db
$db = [];
LEPTON_database::getInstance()->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."mod_procalendar_settings WHERE section_id = ".$section_id ,
	true,
	$db,
	true
);

if (!empty($db)) 
{
   foreach($db as $rec) 
   {
      $resize			= $rec["resize"];
	  $usecustom1		= $rec["usecustom1"];
      $custom1			= $rec["custom1"];
      $customtemplate1	= $rec["customtemplate1"];
      $usecustom2		= $rec["usecustom2"];
      $custom2			= $rec["custom2"];
      $customtemplate2	= $rec["customtemplate2"];
      $usecustom3		= $rec["usecustom3"];
      $custom3			= $rec["custom3"];
      $customtemplate3	= $rec["customtemplate3"];
      $usecustom4		= $rec["usecustom4"];
      $custom4			= $rec["custom4"];
      $customtemplate4	= $rec["customtemplate4"];
      $usecustom5		= $rec["usecustom5"];
      $custom5			= $rec["custom5"];
      $customtemplate5	= $rec["customtemplate5"];
      $usecustom6		= $rec["usecustom6"];
      $custom6			= $rec["custom6"];
      $customtemplate6	= $rec["customtemplate6"];
      $usecustom7		= $rec["usecustom7"];
      $custom7			= $rec["custom7"];
      $customtemplate7	= $rec["customtemplate7"];
      $usecustom8		= $rec["usecustom8"];
      $custom8			= $rec["custom8"];
      $customtemplate8	= $rec["customtemplate8"];
      $usecustom9		= $rec["usecustom9"];
      $custom9			= $rec["custom9"];
      $customtemplate9	= $rec["customtemplate9"];

   }
}
$CTypes['0'] = $MOD_PROCALENDAR['CUSTOM_OPTIONS-0'];
$CTypes['1'] = $MOD_PROCALENDAR['CUSTOM_OPTIONS-1'];
$CTypes['2'] = $MOD_PROCALENDAR['CUSTOM_OPTIONS-2'];
$CTypes['3'] = $MOD_PROCALENDAR['CUSTOM_OPTIONS-3'];
$CTypes['4'] = $MOD_PROCALENDAR['CUSTOM_OPTIONS-4'];

?>

<form name="modify_startday" method="post" action="<?php echo LEPTON_URL; ?>/modules/procalendar/save_customs.php">
  <input type="hidden" name="page_id" value="<?php echo $page_id; ?>">
  <input type="hidden" name="section_id" value="<?php echo $section_id; ?>">
  <input type="hidden" name="type" value="customs">
  <table cellpadding="2" cellspacing="1" border="0" width="98%" class="customfields">
    <tr>
      <td valign="top" colspan="3">
      	<h2><?php echo $MOD_PROCALENDAR['RESIZE_IMAGES']; ?></h2>
      </td>
    </tr>
   
    <?php if(extension_loaded('gd') AND function_exists('imageCreateFromJpeg')) { /* Make's sure GD library is installed */ ?>
    <tr>
      <td><?php echo $TEXT['RESIZE_IMAGE_TO']; ?>:</td>
      <td colspan="2" class="setting_value">
        <select name="resize" style="width: 25%;">
          <option value="0"><?php echo $TEXT['NONE']; ?></option>
            <?php
            $SIZES['50'] = 'Max. 50px';
            $SIZES['75'] = 'Max. 75px';
            $SIZES['100'] = 'Max. 100px';
            $SIZES['125'] = 'Max. 125px';
            $SIZES['150'] = 'Max. 150px';
            $SIZES['175'] = 'Max. 175px';
            $SIZES['200'] = 'Max. 200px';
            $SIZES['225'] = 'Max. 225px';
            $SIZES['250'] = 'Max. 250px';
            foreach($SIZES AS $size => $size_name) {
              if($resize == $size) { $selected = ' selected="selected"'; } else { $selected = ''; }
              echo '<option value="'.$size.'"'.$selected.'>'.$size_name.'</option>';
            }
            ?>
        </select>
      </td>      
    </tr>
    <?php } ?>   
    <tr>
      <td valign="top" colspan="3">
      	<br /><h2><?php echo $MOD_PROCALENDAR['CUSTOMS']; ?></h2>
      </td>
    </tr> 
    <tr>
      <td width="12%">&nbsp;</td>
      <td width="15%"><strong><?php echo $MOD_PROCALENDAR['USE_CUSTOM'];?></strong></td>
      <td><strong><?php echo $MOD_PROCALENDAR['CUSTOM_NAME'];?></strong></td>
    </tr>  
    <tr>
      <td><strong><?php echo $MOD_PROCALENDAR['CUSTOM'];?> 1</strong></td>
      <td>
        <select name="usecustom1" style="width: 98%;">
          <?php
            foreach($CTypes AS $type => $type_name) {
              if($usecustom1 == $type) { $selected = ' selected="selected"'; } else { $selected = ''; }
              echo '<option value="'.$type.'"'.$selected.'>'.$type_name.'</option>';
            }
          ?>
        </select>
      </td>
      <td valign="top"><input name="custom1" class="edit_field_short" type="text" value="<?php echo $custom1; ?>"></td>
    </tr>
    <tr>
      <td valign="top"><?php echo $MOD_PROCALENDAR['CUSTOM_TEMPLATE']; ?></td>
      <td colspan="2" class="setting_value">
        <textarea name="customtemplate1" rows="10" cols="1"><?php echo $customtemplate1; ?></textarea>
      </td>
    </tr>  
    <tr>
      <td><strong><?php echo $MOD_PROCALENDAR['CUSTOM'];?> 2</strong></td>
      <td>
        <select name="usecustom2" style="width:98%;">
          <?php
            foreach($CTypes AS $type => $type_name) {
              if($usecustom2 == $type) { $selected = ' selected="selected"'; } else { $selected = ''; }
              echo '<option value="'.$type.'"'.$selected.'>'.$type_name.'</option>';
            }
          ?>
        </select>
      </td>
      <td valign="top"><input name="custom2" class="edit_field_short" type="text" value="<?php echo $custom2; ?>"></td>
    </tr>
    <tr>
      <td valign="top"><?php echo $MOD_PROCALENDAR['CUSTOM_TEMPLATE']; ?></td>
      <td colspan="2" class="setting_value">
        <textarea name="customtemplate2" rows="10" cols="1"><?php echo $customtemplate2; ?></textarea>
      </td>
    </tr>  
    <tr>
      <td><strong><?php echo $MOD_PROCALENDAR['CUSTOM'];?> 3</strong></td>
      <td>
        <select name="usecustom3" style="width: 98%;">
          <?php
            foreach($CTypes AS $type => $type_name) {
              if($usecustom3 == $type) { $selected = ' selected="selected"'; } else { $selected = ''; }
              echo '<option value="'.$type.'"'.$selected.'>'.$type_name.'</option>';
            }
          ?>
        </select>
      </td>
      <td valign="top"><input name="custom3" class="edit_field_short" type="text" value="<?php echo $custom3; ?>"></td>
    </tr>
    <tr>
      <td valign="top"><?php echo $MOD_PROCALENDAR['CUSTOM_TEMPLATE']; ?></td>
      <td colspan="2" class="setting_value">
        <textarea name="customtemplate3" rows="10" cols="1"><?php echo $customtemplate3; ?></textarea>
      </td>
    </tr>  
    <tr>
      <td><strong><?php echo $MOD_PROCALENDAR['CUSTOM'];?> 4</strong></td>
      <td>
        <select name="usecustom4" style="width: 98%;">
          <?php
            foreach($CTypes AS $type => $type_name) {
              if($usecustom4 == $type) { $selected = ' selected="selected"'; } else { $selected = ''; }
              echo '<option value="'.$type.'"'.$selected.'>'.$type_name.'</option>';
            }
          ?>
        </select>
      </td>
      <td valign="top"><input name="custom4" class="edit_field_short" type="text" value="<?php echo $custom4; ?>"></td>
    </tr>
    <tr>
      <td valign="top"><?php echo $MOD_PROCALENDAR['CUSTOM_TEMPLATE']; ?></td>
      <td colspan="2" class="setting_value">
        <textarea name="customtemplate4" rows="10" cols="1"><?php echo $customtemplate4; ?></textarea>
      </td>
    </tr>  
    <tr>
      <td><strong><?php echo $MOD_PROCALENDAR['CUSTOM'];?> 5</strong></td>
      <td>
        <select name="usecustom5" style="width: 98%;">
          <?php
            foreach($CTypes AS $type => $type_name) {
              if($usecustom5 == $type) { $selected = ' selected="selected"'; } else { $selected = ''; }
              echo '<option value="'.$type.'"'.$selected.'>'.$type_name.'</option>';
            }
          ?>
        </select>
      </td>
      <td valign="top"><input name="custom5" class="edit_field_short" type="text" value="<?php echo $custom5; ?>"></td>
    </tr>
    <tr>
      <td valign="top"><?php echo $MOD_PROCALENDAR['CUSTOM_TEMPLATE']; ?></td>
      <td colspan="2" class="setting_value">
        <textarea name="customtemplate5" rows="10" cols="1"><?php echo $customtemplate5; ?></textarea>
      </td>
    </tr>  
    <tr>
      <td><strong><?php echo $MOD_PROCALENDAR['CUSTOM'];?> 6</strong></td>
      <td>
        <select name="usecustom6" style="width: 98%;">
          <?php
            foreach($CTypes AS $type => $type_name) {
              if($usecustom6 == $type) { $selected = ' selected="selected"'; } else { $selected = ''; }
              echo '<option value="'.$type.'"'.$selected.'>'.$type_name.'</option>';
            }
          ?>
        </select>
      </td>
      <td valign="top"><input name="custom6" class="edit_field_short" type="text" value="<?php echo $custom6; ?>"></td>
    </tr>
    <tr>
      <td valign="top"><?php echo $MOD_PROCALENDAR['CUSTOM_TEMPLATE']; ?></td>
      <td colspan="2" class="setting_value">
        <textarea name="customtemplate6" rows="10" cols="1"><?php echo $customtemplate6; ?></textarea>
      </td>
    </tr>  
    <tr>
      <td><strong><?php echo $MOD_PROCALENDAR['CUSTOM'];?> 7</strong></td>
      <td>
        <select name="usecustom7" style="width: 98%;">
          <?php
            foreach($CTypes AS $type => $type_name) {
              if($usecustom7 == $type) { $selected = ' selected="selected"'; } else { $selected = ''; }
              echo '<option value="'.$type.'"'.$selected.'>'.$type_name.'</option>';
            }
          ?>
        </select>
      </td>
      <td valign="top"><input name="custom7" class="edit_field_short" type="text" value="<?php echo $custom7; ?>"></td>
    </tr>
    <tr>
      <td valign="top"><?php echo $MOD_PROCALENDAR['CUSTOM_TEMPLATE']; ?></td>
      <td colspan="2" class="setting_value">
        <textarea name="customtemplate7" rows="10" cols="1"><?php echo $customtemplate7; ?></textarea>
      </td>
    </tr>  
    <tr>
      <td><strong><?php echo $MOD_PROCALENDAR['CUSTOM'];?> 8</strong></td>
      <td>
        <select name="usecustom8" style="width: 98%;">
          <?php
            foreach($CTypes AS $type => $type_name) {
              if($usecustom8 == $type) { $selected = ' selected="selected"'; } else { $selected = ''; }
              echo '<option value="'.$type.'"'.$selected.'>'.$type_name.'</option>';
            }
          ?>
        </select>
      </td>
      <td valign="top"><input name="custom8" class="edit_field_short" type="text" value="<?php echo $custom8; ?>"></td>
    </tr>
    <tr>
      <td valign="top"><?php echo $MOD_PROCALENDAR['CUSTOM_TEMPLATE']; ?></td>
      <td colspan="2" class="setting_value">
        <textarea name="customtemplate8" rows="10" cols="1"><?php echo $customtemplate8; ?></textarea>
      </td>
    </tr>  
    <tr>
      <td><strong><?php echo $MOD_PROCALENDAR['CUSTOM'];?> 9</strong></td>
      <td>
        <select name="usecustom9" style="width: 98%;">
          <?php
            foreach($CTypes AS $type => $type_name) {
              if($usecustom9 == $type) { $selected = ' selected="selected"'; } else { $selected = ''; }
              echo '<option value="'.$type.'"'.$selected.'>'.$type_name.'</option>';
            }
          ?>
        </select>
      </td>
      <td valign="top"><input name="custom9" class="edit_field_short" type="text" value="<?php echo $custom9; ?>"></td>
    </tr>
    <tr>
      <td valign="top"><?php echo $MOD_PROCALENDAR['CUSTOM_TEMPLATE']; ?></td>
      <td colspan="2" class="setting_value">
        <textarea name="customtemplate9" rows="10" cols="1"><?php echo $customtemplate9; ?></textarea>
      </td>
    </tr> 
    <tr>
      <td align="right" colspan="3"><input class="ui button edit_button" type="submit" value="<?php echo $MOD_PROCALENDAR['SAVE']; ?>"></td>
    </tr>
  </table>
</form>

<br>
<input type="button" class="ui button edit_button" value="<?php echo $MOD_PROCALENDAR['BACK']; ?>" onclick="javascript: window.location = '<?php echo LEPTON_URL."/modules/procalendar/modify_settings.php?page_id=$page_id&amp;section_id=$section_id"; ?>';" />
<?php
$admin->print_footer();

