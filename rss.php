<?php

// Check that GET values have been supplied
if(isset($_GET['page_id']) AND is_numeric($_GET['page_id'])) {
	$page_id = intval($_GET['page_id']);
} else {
	header('Location: /');
	exit(0);
}
// Set defaults
date_default_timezone_set('UTC');
$year  = date('Y', time()); 
$month = date('n', time());

// Editable values
// Show how many items, defaults to 10?
$max   = 100; 

// Set time frame for coming events, default one year
$year2 = $year + 1;
$month2 = $month;

// Include WB files
require_once('../../config/config.php');

$oLEPTON = LEPTON_frontend::getInstance();
$oLEPTON->page_id = $page_id;
$oLEPTON->get_page_details();
$oLEPTON->get_website_settings();

//set charset
$charset=DEFAULT_CHARSET;


// Get page link, needed for linkage
if ($page_id > 0) {
   $result = $database->get_one("SELECT link FROM ".TABLE_PREFIX."pages WHERE page_id = '".$page_id."' ");
   if ( count($result)> 0 ) 
   {
		$page_link = $result;
   }
}

// Sending XML header
header("Content-type: text/xml; charset=".$charset." " );

// Header info
// Required by CSS 2.0
echo '<?xml version="1.0" encoding="'.$charset.'"?>';
?> 
<rss version="2.0">
	<channel>
		<title><?php echo PAGE_TITLE; ?></title>
		<link>http://<?php echo $_SERVER['SERVER_NAME']; ?></link>
		<description> <?php echo PAGE_DESCRIPTION; ?></description>
<?php
// Optional header info 
?>
		<language><?php echo strtolower(DEFAULT_LANGUAGE); ?></language>
		<copyright><?php $thedate = date('Y'); $websitetitle = WEBSITE_TITLE; echo "Copyright {$thedate}, {$websitetitle}"; ?></copyright>
		<managingEditor><?php echo SERVER_EMAIL; ?></managingEditor>
		<webMaster><?php echo SERVER_EMAIL; ?></webMaster>
		<category><?php echo WEBSITE_TITLE; ?></category>
		<generator>LEPTON Content Management System</generator>
<?php


// Set start- and end date for query
$datestart = "$year-$month-1";
$dateend = "$year2-$month2-".cal_days_in_month(CAL_GREGORIAN, $month2,$year2);

// Get terms from database
$terms = [];
$database->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."mod_procalendar_actions WHERE page_id = '".$page_id."' AND date_start <='".$dateend."' AND date_end >='".$datestart."' AND public_stat = 0 ORDER BY date_start,time_start LIMIT 0, ".$max." ",
	true,
	$terms,
	true
);	

//Generating the news items
foreach($terms as $item){ 
	// Build url like : pages/kalendar.php?id=2&detail=1    
	$link = $page_link.'?id='.$item['id'].'&amp;detail=1';
	?>
		<item>
			<title><![CDATA[<?php echo $item["date_start"]." - ".stripslashes($item["name"]); ?>]]></title>
			<description><![CDATA[<?php echo stripslashes($item["description"]); ?>]]></description>
			<guid><?php echo $link; ?></guid>
			<link><?php echo $link; ?></link>
		</item>
<?php } ?>
	</channel>
</rss>