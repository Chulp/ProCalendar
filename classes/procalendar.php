<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


class procalendar extends LEPTON_abstract 
{
	public array $pc_settings = [];
    public string $imageDirectory = "/pro_calendar/";

	public LEPTON_database $database;
	static $instance;
	
	public function initialize () 
	{	
		$this->database = LEPTON_database::getInstance();	
	}	

	public function init_section( $iPageID = 0, $iSectionID = 0 ) {
		
		//get array of settings
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_procalendar_settings WHERE page_id=". $iPageID." AND section_id=". $iSectionID." ",
			true,
			$this->pc_settings,
			false
		);			
	}
}
