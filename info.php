<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$module_directory	= 'procalendar';
$module_name		= 'ProCalendar';
$module_function	= 'page';
$module_version		= '2.4.0';
$module_platform	= '7.x';
$module_author		= 'David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats, erpe, Aldus';
$module_license		= '<a href="https://gnu.org/licenses/gpl-3.0-standalone.html" target="_blank">gnu gpl</a>';
$module_license_terms	= '-';
$module_description	= 'Pro Calendar';
$module_home		= 'https://cms-lab.com';
$module_guid		= '27a8d8b3-1857-4909-a932-97af33c22bda';
