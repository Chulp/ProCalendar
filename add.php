<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

global $page_id, $section_id;

$ct1 = '<div class="field_line">
  <div class="field_title">[CUSTOM_NAME]</div>
  [CUSTOM_CONTENT]
</div>
';

$ct2 = '<div class="field_line">
  <div class="field_title">[CUSTOM_NAME]</div>
  [CUSTOM_CONTENT]
</div>
';

$ct3 = '<div class="field_line">
    <a href="[LEPTONlink[CUSTOM_CONTENT]]">[CUSTOM_NAME]</a>
</div>
';

$ct4 = '<div class="field_line">
    <img src="[CUSTOM_CONTENT]" border ="0" alt="[CUSTOM_NAME]" />
</div>
';

$ct5 = '[[[CUSTOM_CONTENT]]]';
$header = '[CALENDAR]';
$footer = 'Set Calendar Footer in Template';
$posttempl = '<div class="event_details">
  <h2>[NAME]</h2>
  <div class="info_block">
    [DATE_FULL]
    [CUSTOM1]
    [CUSTOM2]
    [CUSTOM3]
    [CUSTOM4]
    [CUSTOM5]
    [CUSTOM6]
    [CATEGORY]
  </div>
[CONTENT]
</div>
[BACK]
';


// insert data into pages table
LEPTON_database::getInstance()->simple_query("INSERT INTO ".TABLE_PREFIX."mod_procalendar_settings SET 
	page_id = '".$page_id."', 
	section_id = '".$section_id."',
	startday = 0,
	usetime = 0,
	useformat = 'dd.mm.yy',
	useifformat = 'd.m.Y',
	customtemplate1 = '".$ct1."',
	usecustom1 = 1,
	custom1 = 'Text field',
	customtemplate2 = '".$ct2."',
	usecustom2 = 2,
	custom2 = 'Text area',
	customtemplate3 = '".$ct3."',
	usecustom3 = 3,
	custom3 = 'Link',
	customtemplate4 = '".$ct4."',
	usecustom4 = 4,
	custom4 = 'Image',
	customtemplate5 = '".$ct5."',
	usecustom5 = 1,
	custom5 = 'Droplet',
	customtemplate6 = '',
	usecustom6 = 0,
	custom6 = '',		
	posttempl = '".$posttempl."',
	header = '".$header."',
	footer = '".$footer."'
");
